const express_jwt = require('express-jwt');
const cookie = require('cookie');

module.exports = (options) => {
  const { jwtPrivateKey } = options;

  const getJwtFromCookie = (req) => {
    if (req.headers.cookie) {
        return cookie.parse(req.headers.cookie).token;
    }
    return null;
  }

  return express_jwt({
    secret: jwtPrivateKey,
    algorithms: ['RS256'],
    requestProperty: 'user',
    credentialsRequired: true,
    getToken: getJwtFromCookie
  });

}