const express = require('express');
const enforce = require('express-sslify');
const bodyParser = require('body-parser')

module.exports = (app) => {
    const appWrapper = express();

    if ("development" === process.env.NODE_ENV) {
        var cors = require('cors');
        appWrapper.use(cors({origin: '*'}));
    } else {
        appWrapper.use(enforce.HTTPS({ trustProtoHeader: true }))
    }
    
    // router.use(bodyParser.json());
    appWrapper.use(bodyParser.urlencoded({ extended: false }));
    
    appWrapper.use(function(req, res, next) {
        console.log(`[${req.method}] ${req.url}`);
        next();
    });

    // connect to main app
    appWrapper.use(app);

    // default route that returns error
    appWrapper.use(function(req, res){
        res.sendStatus(404);
    });

    // default unexpected error handler
    appWrapper.use((err, req, res, next) => {
        console.log('[ERROR] unexpected error at:', req.url)
        console.log('[ERROR]', err.stack)
        res.status(500).send('unexpected error :(');
    });

    var server;
    if ("development" === process.env.NODE_ENV) {
        const fs = require('fs');
        // const https = require('https');
        // var privateKey  = fs.readFileSync('sslcert/key.pem', 'utf8');
        // var certificate = fs.readFileSync('sslcert/cert.pem', 'utf8');
        // var credentials = {key: privateKey, cert: certificate};
        // server = https.createServer(credentials, app);
        server = require('http').createServer(appWrapper);
    }
    else {
        server = appWrapper;
    }

    const PORT = process.env.PORT;
    server.listen(PORT, () => console.log(`Listening on ${ PORT }`))

}
