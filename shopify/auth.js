const cookie = require('cookie');
const express = require('express');
const asyncHandler = require('express-async-handler')
const jwt = require('jsonwebtoken');
const roboshopJwt = require('roboshop-common/roboshop-express-jwt');
const Shopify = require('shopify-api-node');

getShopifyApi = (name, token) => {
    return new Shopify({
        shopName: name,
        accessToken: token,
        autoLimit: true,
        apiVersion: process.env.SHOPIFY_API_VERSION
    });
}

const updateModelOnLogin = async (model, shopId, shopName, accessToken, baseUrl) => {
    // checkIfInstalled
    const storeInfo = await model.findByPk(shopId)
    .catch((error) => {
        console.log('[ERROR] callback Store database query failed:', error.stack);
        return res.sendStatus(500);
    });

    let isInstall = false;

    if (null === storeInfo) {
        console.log(`[INFO] New Install: ${shopName}`)
        isInstall = true;

        try {
            await model.upsert({
                id     : shopId,
                name   : shopName,
                token  : accessToken,
                active : true
            })
        } catch (error) {
            console.log('[ERROR] Failed writing store info in database for store', shopName, ':', error.stack);
            return res.sendStatus(500);
        }
        // registerUninstallWebhook(shopify, `https://${process.env.HOST_URL}/${baseUrl}/webhooks/app/uninstalled`);
        console.log(`[INFO] New store saved in database: ${shopName}`)
    }
    else if (accessToken != storeInfo.token) {
        console.log(`[INFO] Reinstall: ${shopName}`);
        isInstall = true;

        try {
            await storeInfo.update({
                token : accessToken,
                active: true
            });
        }
        catch (error) {
            console.log('[ERROR] Failed updating store info in database for store', shopName, ':', error.stack);
            return res.sendStatus(500);
        };
        console.log('[INFO] store token updated successfully')
    }
    else {
        console.log(`[INFO] store already in database: ${shopName}`)
    }

    return isInstall;
}


module.exports = (options) => {
    var { shopifyToken, jwtPrivateKey, jwtPublicKey, model, onLogin, authRoute, cbRoute, clientRoute } = options;
    clientRoute = clientRoute || '/';

    const router = express.Router();

    redirect = (req, res) => {
        const url = req.baseUrl + clientRoute;
        console.log(`redirecting to: ${url}`)
        return res.redirect(url);
    }

    auth = (req, res, next) => {
        // if (req.headers.cookie && cookie.parse(req.headers.cookie).token) {
        //     try {
        //         var decoded = jwt.verify(cookie.parse(req.headers.cookie).token, jwtPublicKey, { algorithm: 'RS256' });
        //     } catch (err) {
        //         console.log('JWT verify failed:', err.message);
        //         // res.clearCookie('token');
        //         res.cookie('token', null, { sameSite: 'none', secure: true, expires: new Date(0) });
        //         decoded = undefined;
        //     }
        //     if (decoded) {
        //         console.log('[INFO] /auth: already authenticated.', decoded)
        //         return next();
        //     }
        // }
    
        const shop = req.query.shop;
        if (shop) {
            console.log('[INFO] /auth: shopify token request for', shop)
            const state = shopifyToken.generateNonce();
            const installUri = shopifyToken.generateAuthUrl(shop, undefined, state);
            console.log('[INFO] /auth: redirecting to: ', installUri)
            res.cookie('state', state, { sameSite: 'none', secure: true, path: req.baseUrl });
            res.redirect(installUri);
        } else {
            console.log('[ERROR] /auth: shop url is missing')
            return res.status(400).send('Missing shop parameter. Please add ?shop=your-development-shop.myshopify.com to your request');
        }
    }

    callback = asyncHandler(async (req, res, next) => {
        const stateCookie = cookie.parse(req.headers.cookie).state;
        const state = req.query.state;
        if (
            typeof state !== 'string'
            || state !== stateCookie                // Validate the state.
            || !shopifyToken.verifyHmac(req.query)  // Validare the hmac.
        ) {
            const message = 'Request origin cannot be verified'
            console.log('[ERROR]', message)
            return res.status(403).send(message);
        }
    
        const accessTokenResponse = await shopifyToken.getAccessToken(req.query.shop, req.query.code)
        .catch((error) => {
            const message = 'Failed to get access token.'
            console.log('[ERROR]', message, ':', error.stack);
            return res.status(401).send(message);
        });
        const accessToken = accessTokenResponse.access_token;
        console.log('[INFO] callback accessTokenResponse received successfully. store', req.query.shop, 'token', accessToken);

        const shopify = getShopifyApi(req.query.shop, accessToken);
        var store = await shopify.shop.get({
            fields: [ 'id' ]
        })
        .catch(error => {
            console.log('[ERROR] callback read store info failed for store', req.query.shop, ':', error.stack)
            return res.sendStatus(401).message('Failed to get shopify store data');
        })
        console.log('[INFO] callback read store info successfully');
        
        if (model) {
            const isInstall = await updateModelOnLogin(model, store.id, req.query.shop, accessToken, req.baseUrl);
            if (isInstall && onInstall) {
                await onInstall(store.id, req.query.shop, accessToken, req.baseUrl);
            }
        }
        if (onLogin) {
            await onLogin(store.id, req.query.shop, accessToken, req.baseUrl);
        }
        
        console.log(`[INFO] Authentication done for ${req.baseUrl}`);
        var token = jwt.sign({ id: store.id, shop: req.query.shop}, jwtPrivateKey, { algorithm: 'RS256', expiresIn: '1d' });
        res.cookie('state', null, { sameSite: 'none', secure: true, path: req.baseUrl || '/', expires: new Date(0) });
        res.cookie('token', token, { sameSite: 'none', secure: true, path: req.baseUrl || '/' });
        next();
    });

    router.get(authRoute, auth, redirect);
    router.get(cbRoute, callback, redirect);

    // Lock route with JWT
    router.use(roboshopJwt({jwtPrivateKey: jwtPrivateKey}));

    return router;
}



