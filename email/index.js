const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');

const {
  REPORTS_MAIL_SERVER,
  REPORTS_MAIL_PORT,
  REPORTS_MAIL_USERNAME,
  REPORTS_MAIL_PASSWORD,
  REPORTS_MAIL_RECEPIENTS
} = process.env;

let reporterTransport = nodemailer.createTransport({
    host: REPORTS_MAIL_SERVER,
    port: REPORTS_MAIL_PORT,
    secure: true,
    debug: false,
    auth: {
        user: REPORTS_MAIL_USERNAME,
        pass: REPORTS_MAIL_PASSWORD
    },
    tls: {
        rejectUnauthorized: false
    }
});

reporterTransport.use('compile', hbs({
  viewPath: 'views/email/reporter',
  extName: '.hbs'
}));

const sendMail = async (options) => {
  try {
    // await reporterTransport.verify();
    let info = await reporterTransport.sendMail(options);
  } catch (err) {
    console.log(err);
  }
}

module.exports.sendInstallEmail = async (appName, store) => {
  sendMail({
    from: '"RoboshopReporter" <reports@roboshop.app>',
    to: REPORTS_MAIL_RECEPIENTS.split(','),
    subject: `[RoboShop] ${appName} Installed`,
    template: 'install',
    context: Object.assign({
      appName: appName
    }, store.dataValues)
  });
}

module.exports.sendUninstallEmail = async (appName, store) => {
  sendMail({
    from: '"RoboshopReporter" <reports@roboshop.app>',
    to: REPORTS_MAIL_RECEPIENTS.split(','),
    subject: `[RoboShop] ${appName} Uninstalled`,
    template: 'uninstall',
    context: Object.assign({
      appName: appName
    }, store.dataValues)
  });
}

