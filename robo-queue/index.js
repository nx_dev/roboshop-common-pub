const Queue = require('bull');

const workerQueue = new Queue('worker', process.env.REDIS_URL);

module.exports.add = (options) => {
    const { appName, shopId, domain, accessToken, baseUrl, isReadAllOrders } = options;
    console.log(`[INFO][roboQueue] sending request for ${shopId}`)
    workerQueue.add({
        appName: appName,
        shopId: shopId,
        domain: domain,
        accessToken: accessToken,
        baseUrl: baseUrl,
        isReadAllOrders: isReadAllOrders
    });
}

module.exports.listen = (fn) => {
    workerQueue.process((job) => {
        const { appName, shopId, domain, accessToken, baseUrl, isReadAllOrders } = job.data;
        console.log(`[INFO][roboQueue] Got process request for ${shopId}`)
        fn( appName, shopId, domain, accessToken, baseUrl, isReadAllOrders );
    });
}
