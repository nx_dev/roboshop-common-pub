const crypto = require('crypto');

module.exports.generatePassword = (length = 16) => {
  var uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  var lowercase = 'abcdefghijklmnopqrstuvwxyz';
  var numbers = '0123456789';
  var symbols = '!"#$%&\'()*+,-./;<=>?@^[\\]^_`{|}~';

  var all = Buffer.from(uppercase + lowercase + numbers + symbols);

  var indices = crypto.randomBytes(length);
  var password = indices.map(i => all[i % all.length]).toString();

  return password;
}

module.exports.createBasicAuth = (user, pass) => {
  return Buffer.from(user + ":" + pass).toString('base64')
}

